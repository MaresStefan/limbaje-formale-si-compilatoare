#include "AFN.h"
#include<algorithm>
#include<iterator>
#include<list>

std::istream & operator>>(std::istream & is, AFN & afn)
{
	is >> afn.sigma;
	int nrStari = 0;
	int nrStariFinale = 0;
	int nrLegaturi = 0;

	is >> nrStari;
	afn.stari.reserve(nrStari);
	std::string stare;
	for (int i = 0; i < nrStari; i++)
	{
		is >> stare;
		afn.stari.emplace_back(stare);
	}

	is >> afn.stareInitiala;

	is >> nrStariFinale;
	afn.stariFinale.reserve(nrStariFinale);
	for (int i = 0; i < nrStariFinale; i++)
	{
		is >> stare;
		afn.stariFinale.emplace_back(stare);
	}

	is >> nrLegaturi;
	Legatura legatura;
	for (int i = 0; i < nrLegaturi; i++)
	{
		is >> stare;
		is >> legatura;
		afn.delta[stare].push_back(legatura);
	}

	return is;
}

std::ostream & operator<<(std::ostream & os, const AFN & afn)
{
	os << "Alfabet: " << afn.sigma << std::endl;

	os << "Stari: ";
	std::ostream_iterator<std::string> out(os, " ");
	std::copy(afn.stari.begin(), afn.stari.end(), out);

	os << std::endl << "Satre initiala: " << afn.stareInitiala << std::endl;

	os << "Stari finale: ";
	std::copy(afn.stariFinale.begin(), afn.stariFinale.end(), out);

	os << std::endl << "Delta: " << std::endl;
	for (auto d : afn.delta)
	{
		for (auto l : d.second)
		{
			os << "( " << d.first << " , " << l << std::endl;
		}
	}
	return os;
}

void AFN::verificare(const std::string & cuvant)
{
	std::string stareCurenta = this->stareInitiala;
	int index = 0;
	std::vector<std::string> stariCurente1, stariCurente2;
	stariCurente1.push_back(stareCurenta);
	while (index != cuvant.size())
	{
		for (std::string stare : stariCurente1)
		{

			for (Legatura legatura : delta[stare])
			{
				if (legatura.simbol == cuvant[index])
				{
					if (std::find(stariCurente2.begin(), stariCurente2.end(), legatura.end) == stariCurente2.end())
					{
						stariCurente2.push_back(legatura.end);
					}
				}
			}
		}
		index++;

		stariCurente1 = stariCurente2;
		stariCurente2.clear();

	}
		if (stariCurente1.empty())
		{
			std::cout << "Blocaj!" << std::endl;
		}
		else
		{
			std::vector<std::string> temp;
			sort(stariCurente1.begin(), stariCurente1.end());
			sort(this->stariFinale.begin(), this->stariFinale.end());
			std::set_intersection(stariCurente1.begin(), stariCurente1.end(), stariFinale.begin(), stariFinale.end(), std::back_inserter(temp));
			if (temp.size() == 0)
			{
				std::cout << "Neacceptat!";
			}
			else
			{
				std::cout << "Acceptat!";
			}
		}
}
