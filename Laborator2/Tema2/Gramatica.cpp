#include "Gramatica.h"
#include<algorithm>


bool Gramatica::verifyGramatics()
{
	if (intersectieTerminaleNeterminale()) 
	{
		if (simbolStartInVN())
		{
			if (unNeterminaInMembrulStang())
			{
				if (atLeastAProductionWithStartSimbol())
				{
					if (onlyElementsFromVTandVN())
					{
						return true;
					}
					else
					{
						std::cout << "Exista simboluri straine."<<std::endl;
						return false;
					}
				}
				else
				{
					std::cout << "Nici o productie nu incepe cu simbolul de start."<<std::endl;
					return false;
				}
			}
			else
			{
				std::cout << "Exista productii fara neterminale in membrul stang"<<std::endl;
				return false;
			}
		}
		else
		{
			std::cout << "Simbolul de start nu se gaseste in VN"<<std::endl;
			return false;
		}
	}
	else
	{
		std::cout << "VN se intersecteaza in VT" << std::endl;
		return false;
	}
	
}

bool Gramatica::intersectieTerminaleNeterminale()
{
	std::string temp;
	std::sort(VN.begin(), VN.end());
	std::sort(VT.begin(), VT.end());
	std::set_intersection(VN.begin(),VN.end(), VT.begin(), VT.end(), std::back_inserter(temp));
	if(temp.empty())
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Gramatica::simbolStartInVN()
{
	for (auto chr : VN)
	{
		if (this->S == chr)
			return true;
	}
	return false;
}

bool Gramatica::unNeterminaInMembrulStang()
{
	for (auto productie : productii)
	{
		bool ok = false;
		for (auto chr : productie.Simbol)
		{
			if (VN.find(chr) != std::string::npos)
			{
				ok = true;
				break;
			}
		}
		if (ok == false)
		{
			return false;
		}
	}
	return true;
	
}

bool Gramatica::atLeastAProductionWithStartSimbol()
{
	for (auto productie : productii)
	{
		if (productie.Simbol.size()==1 && productie.Simbol[0]==S)
		{
			return true;
		}
	}
	return false;
}

bool Gramatica::onlyElementsFromVTandVN()
{
	for (auto productie : productii)
	{
		for (auto chr : productie.result)
		{
			if (VN.find(chr) == std::string::npos && VT.find(chr) == std::string::npos)
			{
				return false;
			}
		}

		for (auto chr : productie.Simbol)
		{
			if (VN.find(chr) == std::string::npos && VT.find(chr) == std::string::npos)
			{
				return false;
			}
		}
	}
	return true;
}

void Gramatica::generate(uint8_t choice)
{
	std::deque<Productie> aplicabile;
	std::string currentWord;
	currentWord+=S;
	if (choice == 1)
	{
		int i = 0;
		do
		{
			//clean aplicabile
			if (!aplicabile.empty())
			{
				aplicabile.clear();
			}

			//marcare productii aplicabile
			for (auto productie : productii)
			{
				if (aplicabila(productie, currentWord))
				{
					aplicabile.push_back(productie);
				}
			}
			//alegere una random
			int selectedProd = rand() % aplicabile.size();

			//aplicare
			
			Productie randomChoosen = aplicabile[selectedProd];
			int found = currentWord.find(randomChoosen.Simbol);
			std::cout << currentWord << " -> ";
			currentWord.replace(found, randomChoosen.Simbol.size(), randomChoosen.result);
			std::cout << currentWord << std::endl;
			i++;

		} while (!aplicabile.empty() && !justTerminals(currentWord));

		if (justTerminals(currentWord))
		{
			std::cout << "Cuvant corect!";
		}
		else
		{
			std::cout << "Incorect";
		}
	}
	else
	{
		do
		{

			//clean aplicabile
			if (!aplicabile.empty())
			{
			aplicabile.clear();
			}


			//marcare productii aplicabile
			for (auto productie : productii)
			{
				if (aplicabila(productie, currentWord))
				{
					aplicabile.push_back(productie);
				}
			}
			//alegere una random
			int selectedProd = rand() % aplicabile.size();

			//aplicare

			Productie randomChoosen = aplicabile[selectedProd];
			int found = currentWord.find(randomChoosen.Simbol);
			currentWord.replace(found, randomChoosen.Simbol.size(), randomChoosen.result);

		} while (!aplicabile.empty() && !justTerminals(currentWord));
		if (justTerminals(currentWord))
		{
			std::cout << currentWord << std::endl;
			std::cout << "Cuvant corect!";
		}
		else
		{
			std::cout << "Incorect";
		}
	}

}

bool Gramatica::aplicabila(const Productie & productie, const std::string & currentWord)
{
	if (currentWord.find(productie.Simbol) != std::string::npos)
	{
		return true;
	}
	return false;
}

bool Gramatica::justTerminals(const std::string & word)
{
	for (auto chr : word)
	{
		if (VN.find(chr) != std::string::npos)
		{
			return false;
		}
	}
	return true;
}

std::istream & operator>>(std::istream & is, Gramatica & gram)
{
	is >> gram.VN;
	is >> gram.VT;
	is >> gram.S;
	is >> gram.numberOfProducts;
	gram.productii.reserve(gram.numberOfProducts);
	Productie p;
	for (int i = 0; i < gram.numberOfProducts; i++)
	{
		is >> p;
		gram.productii.emplace_back(p);
	}
	return is;
}

std::ostream & operator<<(std::ostream & os, const Gramatica & gram)
{
	os << gram.VN << std::endl;
	os << gram.VT << std::endl;
	for (auto productie : gram.productii)
	{
		os << productie << std::endl;
	}
	return os;
}
