#pragma once
#include<string>
#include<vector>
#include<iostream>
#include<ctime>
#include<regex>
#include<deque>
struct Productie
{
	std::string Simbol;
	std::string result;
	friend std::istream& operator>>(std::istream& is, Productie& p)
	{
		is >> p.Simbol;
		is >> p.result;
		if (p.result == "lambda")
		{
			p.result = "";
		}
		return is;
	}

	friend std::ostream& operator<<(std::ostream& os, const Productie& p)
	{
		if (p.result == "")
		{
			os << p.Simbol << "->" << "lambda";
		}
		else
		{
			os << p.Simbol << " -> " << p.result;
		}
		return os;
	}
};
class Gramatica
{
public:

	friend std::istream& operator >> (std::istream& is, Gramatica& gram);
	friend std::ostream& operator << (std::ostream& os, const Gramatica& gram);

	bool verifyGramatics();
	bool intersectieTerminaleNeterminale();
	bool simbolStartInVN();
	bool unNeterminaInMembrulStang();
	bool atLeastAProductionWithStartSimbol();
	bool  onlyElementsFromVTandVN();
	void generate(uint8_t choice);
	bool aplicabila(const Productie& productie, const std::string& word);
	bool justTerminals(const std::string& word);

private:
	std::string VN;
	std::string VT;
	char S;
	int numberOfProducts;
	std::vector<Productie> productii;
};

