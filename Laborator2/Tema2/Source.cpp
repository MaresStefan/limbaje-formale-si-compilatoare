#include"Gramatica.h"
#include<fstream>
#include<algorithm>
int main()
{
	Gramatica g;
	srand(time(NULL));
	std::fstream file("Gramatica.txt");
	file >> g;
	if (g.verifyGramatics())
	{
		std::cout << "Good\n";

		for (size_t i = 0; i < 10; i++)
		{
			std::cout << std::endl;
			std::cout << "Generation: " << i << std::endl;
			g.generate(0);
			std::cout << std::endl;
		}

		std::cout << "Done";
	}

	
	system("pause");
}