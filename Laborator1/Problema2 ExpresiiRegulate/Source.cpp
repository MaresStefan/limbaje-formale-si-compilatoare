#include<iostream>
#include<regex>
#include<string>


int main()
{
	std::regex patternIntegerNumber("[+-]?\\d+");
	std::regex patternNaturalNumber("[1-9]\\d*");
	std::regex patternRealNumber(R"([+-]?\d+?\.?\d+?)");
	std::string str = "2.3";

	if (std::regex_match(str, patternRealNumber))
	{
		std::cout << "Match!";
	}

	system("pause");
}