#include<iostream>
#include<vector>
#include<random>
#include<string>
#include<fstream>
#include<ctime>


void drawHead()
{
	std::cout << "+-------------------+\n";
	std::cout << "|                   |\n";
	std::cout << "|        ____       |\n";
	std::cout << "|       |    |      |\n";
	std::cout << "|       |    O      |\n";
	std::cout << "|       |           |\n";
	std::cout << "|       |           |\n";
	std::cout << "|     __|___        |\n";
	std::cout << "|    |      |       |\n";
	std::cout << "|    |______|       |\n";
}

void drawBody()
{
	std::cout << "+-------------------+\n";
	std::cout << "|                   |\n";
	std::cout << "|        ____       |\n";
	std::cout << "|       |    |      |\n";
	std::cout << "|       |    O      |\n";
	std::cout << "|       |    |      |\n";
	std::cout << "|       |           |\n";
	std::cout << "|     __|___        |\n";
	std::cout << "|    |      |       |\n";
	std::cout << "|    |______|       |\n";
}

void drawLeftArm()
{
	std::cout << "+-------------------+\n";
	std::cout << "|                   |\n";
	std::cout << "|        ____       |\n";
	std::cout << "|       |    |      |\n";
	std::cout << "|       |    O      |\n";
	std::cout << "|       |   /|      |\n";
	std::cout << "|       |           |\n";
	std::cout << "|     __|___        |\n";
	std::cout << "|    |      |       |\n";
	std::cout << "|    |______|       |\n";
}

void drawRightArm()
{
	std::cout << "+-------------------+\n";
	std::cout << "|                   |\n";
	std::cout << "|        ____       |\n";
	std::cout << "|       |    |      |\n";
	std::cout << "|       |    O      |\n";
	std::cout << "|       |   /|\\     |\n";
	std::cout << "|       |           |\n";
	std::cout << "|     __|___        |\n";
	std::cout << "|    |      |       |\n";
	std::cout << "|    |______|       |\n";
}

void drawLeftFoot()
{
	std::cout << "+-------------------+\n";
	std::cout << "|                   |\n";
	std::cout << "|        ____       |\n";
	std::cout << "|       |    |      |\n";
	std::cout << "|       |    O      |\n";
	std::cout << "|       |   /|\\     |\n";
	std::cout << "|       |   /       |\n";
	std::cout << "|     __|___        |\n";
	std::cout << "|    |      |       |\n";
	std::cout << "|    |______|       |\n";
}

void drawRightFoot()
{
	std::cout << "+-------------------+\n";
	std::cout << "|                   |\n";
	std::cout << "|        ____       |\n";
	std::cout << "|       |    |      |\n";
	std::cout << "|       |    O      |\n";
	std::cout << "|       |   /|\\     |\n";
	std::cout << "|       |   / \\     |\n";
	std::cout << "|     __|___        |\n";
	std::cout << "|    |      |       |\n";
	std::cout << "|    |______|       |\n";
}

void drawHanging()
{
	std::cout << "+-------------------+\n";
	std::cout << "|                   |\n";
	std::cout << "|        ____       |\n";
	std::cout << "|       |    |      |\n";
	std::cout << "|       |           |\n";
	std::cout << "|       |           |\n";
	std::cout << "|       |           |\n";
	std::cout << "|     __|___        |\n";
	std::cout << "|    |      |       |\n";
	std::cout << "|    |______|       |\n";
}

void drawHiddenWord(const std::string& hWord)
{
	std::cout << std::endl;
	std::cout << "Cuvantul ascuns:	";
	for (auto ch : hWord)
	{
		std::cout << ch << " ";
	}
		std::cout << std::endl;
}

void printUsedLetters(const std::vector<char>& usedLetters)
{
	std::cout << std::endl << "Litere deja folosite: ";
	for (auto c : usedLetters)
	{
		std::cout << c << " ";
	}
}

bool foundInUsed(const std::vector<char>& usedLetters, const char& letter)
{
	for (char c : usedLetters)
	{
		if (letter == c)
		{
			return true;
		}
	}
	return false;
}


int main()
{
	srand(time(NULL));
	std::ifstream file("Words.txt");
	std::vector<std::string> words;
	int numberOfWords = 0;
	file >> numberOfWords;
	words.reserve(numberOfWords);
	std::string word;

	for (int i = 0; i < numberOfWords; ++i)
	{
		file >> word;
		words.emplace_back(word);
	}

	int random = rand() % words.size();
	word = words[random];

	std::string hiddenWord=word;
	std::vector<char> usedLetters;
	int life = 6;
	bool win = false;

	//mask the hidden word
	for (int i=0;i<hiddenWord.size();i++)
	{
		hiddenWord[i] = '_';
	}

	while (life >= 0 && !win)
	{
		switch (life)
		{
		case 6:
			drawHanging();
			printUsedLetters(usedLetters);
			drawHiddenWord(hiddenWord);
			break;
		case 5:
			drawHead();
			printUsedLetters(usedLetters);
			drawHiddenWord(hiddenWord);
			break;
		case 4:
			drawBody();
			printUsedLetters(usedLetters);
			drawHiddenWord(hiddenWord);
			break;
		case 3:
			drawLeftArm();
			printUsedLetters(usedLetters);
			drawHiddenWord(hiddenWord);
			break;
		case 2:
			drawRightArm();
			printUsedLetters(usedLetters);
			drawHiddenWord(hiddenWord);
			break;
		case 1:
			drawLeftFoot();
			printUsedLetters(usedLetters);
			drawHiddenWord(hiddenWord);
			break;
		case 0:
			drawRightFoot();
			std::cout << "AI PIERDUT! HAHA"<<std::endl;
			std::cout << "Cuvantul era: " << word << std::endl;
			--life;
			system("pause");
			break;
		default:
			break;
		}

		if (word == hiddenWord)
		{
			std::cout << "Bravo! Ai castigat!" << std::endl;
			win = true;
			system("pause");
			break;
		}

		if (life > 0)
		{
			char letter = '\0';
			std::cout << "Introdu o litera: ";
			std::cin >> letter;
			if (!isupper(letter))
			{
				letter = toupper(letter);
			}
			while (!isalpha(letter))
			{
				//std::cout << "Introduceti o litera valida: ";
				std::cin >> letter;
				if (!isupper(letter))
				{
					letter = toupper(letter);
				}
			}

			while (foundInUsed(usedLetters, letter))
			{
				std::cout << std::endl << "Litera deja folosita!" << std::endl << "Introduceti o alta litera: ";
				std::cin >> letter;
				if (!isupper(letter))
				{
					letter = toupper(letter);
				}
			}

			usedLetters.push_back(letter);

			bool found = false;
			for (int i = 0; i < word.size(); i++)
			{
				if (word[i] == letter)
				{
					found = true;
					hiddenWord[i] = letter;
				}
			}
			if (!found)
			{
				--life;
			}
		}
		system("cls");
	}
}