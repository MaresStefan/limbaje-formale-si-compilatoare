#include "AFD.h"
#include<algorithm>
#include<iterator>
std::istream & operator>>(std::istream & is, AFD & afd)
{
	is >> afd.sigma;
	int nrStari = 0;
	int nrStariFinale = 0;
	int nrLegaturi = 0;

	is >> nrStari;
	afd.stari.reserve(nrStari);
	std::string stare;
	for (int i = 0; i < nrStari; i++)
	{
		is >> stare;
		afd.stari.emplace_back(stare);
	}

	is >> afd.stareInitiala;
	
	is >> nrStariFinale;
	afd.stariFinale.reserve(nrStariFinale);
	for (int i = 0; i < nrStariFinale; i++)
	{
		is >> stare;
		afd.stariFinale.emplace_back(stare);
	}

	is >> nrLegaturi;
	Legatura legatura;
	for (int i = 0; i < nrLegaturi; i++)
	{
		is >> stare;
		is >> legatura;
		afd.delta[stare].push_back(legatura);
	}

	return is;
}

std::ostream & operator<<(std::ostream & os, const AFD & afd)
{
	os << "Alfabet: " << afd.sigma<<std::endl;

	os << "Stari: ";
	std::ostream_iterator<std::string> out(os, " ");
	std::copy(afd.stari.begin(), afd.stari.end(), out);

	os <<std::endl<< "Satre initiala: " << afd.stareInitiala << std::endl;

	os << "Stari finale: ";
	std::copy(afd.stariFinale.begin(), afd.stariFinale.end(), out);

	os <<std::endl<< "Delta: " << std::endl;
	for (auto d : afd.delta)
	{
		for (auto l : d.second)
		{
			os << "( " << d.first << " , " << l << std::endl;
		}
	}
	return os;
}

void AFD::verificare(const std::string & cuvant)
{
	std::string stareCurenta = stareInitiala;
	int index = 0;
	while (cuvant.size() != index)
	{
		bool found = false;
		for (Legatura legatura : delta[stareCurenta])
		{
			if (legatura.simbol == cuvant[index])
			{
				stareCurenta = legatura.end;
				++index;
				found = true;
				break;
			}
		}
		if (!found)
		{
			std::cout << "Blocaj!"<<std::endl;
			return;
		}
	}
	
	
	if (std::find(stariFinale.begin(), stariFinale.end(), stareCurenta) == stariFinale.end())
	{
		std::cout << "Cuvant neacceptat" << std::endl;
	}
	else
	{
		std::cout << "Cuvand acceptat!" << std::endl;
	}
}
