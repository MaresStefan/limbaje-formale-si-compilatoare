#pragma once
#include<iostream>
#include<string>
#include<vector>
#include<map>

struct Legatura
{
	Legatura()
	{
		this->end = "";
		this->simbol = ' ';
	}
	Legatura(const std::string& end, const char& simbol)
	{
		this->end = end;
		this->simbol = simbol;
	}

	friend std::istream& operator>>(std::istream& is, Legatura& legatura)
	{
		is >> legatura.simbol;
		is >> legatura.end;
		return is;
	}

	friend std::ostream& operator<<(std::ostream& os, const Legatura& legatura)
	{
		os << legatura.simbol << " ) -> " << legatura.end;
		return os;
	}
	std::string end;
	char simbol;
};
class AFD
{
	friend std::istream& operator>>(std::istream& is, AFD& afd);
	friend std::ostream& operator<<(std::ostream& os, const AFD& afd);
public:
	void verificare(const std::string& cuvant);

private:
	std::string sigma;
	std::vector<std::string> stari;
	std::string stareInitiala;
	std::map<std::string, std::vector<Legatura>> delta;
	std::vector<std::string> stariFinale;
};